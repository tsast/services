const fs = require('fs');
/**
 * Require js files from dir to object (sync)
 * @type {Function}
 * @memberOf services
 * @prop {String} dir - Start directory
 * @prop {Array} [ignore=[]] - Ignore files array
 * @return {Object} required object key = fileName without ext, value required file
 */
function includeFilesFromDir(dir, ignore) {
    const obj = {};
    ignore = ignore || [];
    const list = fs.readdirSync(dir);
    list.forEach((v) => {
        const cd = `${dir}/${v}`;
        const f = fs.lstatSync(cd);
        if (f.isFile()) {
            if (ignore.indexOf(v) === -1) {
                obj[v.replace(`.${v.split('.').pop()}`, '')] = require(cd);
            }
        }
    });
    return obj;
}
module.exports = includeFilesFromDir;
