const mime = require('mime-types');
const fs = require('fs');
const path = require('path');
/**
 * Get files encoded to base64 from dir
 * @type {Function}
 * @memberOf services
 * @prop {String} dir - files dir
 * @return {Object} key filename, value file encoded base64
 */
function filesToBase64FromDir(dir) {
    const files = {};
    fs.readdirSync(dir).forEach((file) => {
        const p = path.join(dir, file);
        const f = fs.statSync(p);
        const ext = file.split('.').pop();
        const fn = file.replace(`.${ext}`, '');
        if (f.isFile() && fn.length > 0) {
            files[fn] = `data:${mime.lookup(ext)};base64,${fs.readFileSync(p).toString('base64')}`;
        }
    });
    return files;
}
module.exports = filesToBase64FromDir;
