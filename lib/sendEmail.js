'use strict';
const EmailTemplate = require('email-templates').EmailTemplate;
const email = require('emailjs');
const Handlebars = require('handlebars');
const request = require('request');
const fs = require('fs');
const path = require('path');
/**
 * Sending emails
 * @type {Function}
 * @memberOf services
 * @prop {String} To - Email
 * @prop {String} Subject - Subject
 * @prop {String} templatePath - Template path [templates docs]{@link https://www.npmjs.org/package/email-templates}
 * @prop {String} data - Template data
 * @prop {Array} [attachment=undefined] - Attachment array
 * @prop {String} attachment.path - Path to file
 * @prop {String} attachment.type - File content type
 * @prop {String} attachment.name - File name
 * @prop {Beanstalkd} Beanstalkd - Beanstalkd
 * @prop {Number} entryId - Entry ID
 * @returns {Promise} Result or Catch
 */
function sendEmail(To, Subject, templatePath, data, attachment, Beanstalkd, entryId) {
    entryId = entryId || null;
    return new Promise((resolve, reject) => {
        const settings = Object.assign({}, config.email);
        delete settings.from;
        const server = email.server.connect(settings);
        Handlebars.registerHelper('addOne', value => (value + 1));
        Handlebars.registerHelper('is', (a, b, opts) => {
            if (a === b) {
                return opts.fn(this);
            }
            return opts.inverse(this);
        });
        const message = new EmailTemplate(templatePath);
        return message.render(data, (err, msg) => {
            if (err) return reject(err);
            if (!Beanstalkd) {
                const Message = {
                    from: config.email.from,
                    to: To,
                    subject: Subject,
                    text: msg.text,
                    attachment: []
                };
                if (msg.hasOwnProperty('html') && msg.html && msg.html.length > 0) {
                    Message.attachment.push({
                        data: msg.html,
                        alternative: true
                    });
                }
                if (attachment && attachment.length > 0) {
                    Message.attachment = Message.attachment.concat(attachment);
                }
                return server.send(Message, (errSend) => {
                    if (errSend) return reject(errSend);
                    return resolve(Message);
                });
            }
            const files = [];
            // Save txt message
            const fileText = path.join(__dirname, `${Math.random().toString(16)}-text.txt`);
            files.push({
                name: 'msg-text.txt',
                type: 'text/plain',
                path: fileText,
                fType: 1
            });
            try {
                fs.writeFileSync(fileText, msg.text);
            } catch (e) {
                files.pop();
                console.error(e);
            }
            // Save html message
            if (msg.html && msg.html.length > 0) {
                const fileHtml = path.join(__dirname, `${Math.random().toString(16)}-html.html`);
                files.push({
                    name: 'msg-html.html',
                    type: 'text/html',
                    path: fileHtml,
                    fType: 2
                });
                try {
                    fs.writeFileSync(fileHtml, msg.html);
                } catch (e) {
                    files.pop();
                    console.error(e);
                }
            }
            // Add attachment to upload list
            attachment.forEach(v => {
                try {
                    files.push(Object.assign(v, {
                        fType: 3
                    }));
                } catch (e) {
                    console.error(e);
                }
            });
            let rqS = null;
            let id = null;
            return new mssql.Connection(config.db.ms).connect()
            .then(connect => {
                rqS = new mssql.Request(connect);
                if (entryId) {
                    const query = `
                    INSERT INTO Emails
                        ("entryId", "email", "subject", "data", "createdAt")
                    OUTPUT Inserted.id
                    VALUES
                        (${entryId}, '${To}', '${Subject}', '${JSON.stringify(data)}', getdate())
                    `;
                    return rqS.query(query);
                }
                return true;
            })
            .then(idA => {
                if (entryId) {
                    id = idA[0].id;
                }
                return Promise.mapSeries(files, file => {
                    const formData = {
                        file: fs.createReadStream(file.path)
                    };
                    return new Promise(resolveP => {
                        request.post({
                            url: config.integrations.storage.url,
                            formData,
                            headers: {
                                token: config.integrations.storage.token
                            }
                        }, (errRequest, httpResponse, body) => {
                            try {
                                fs.unlinkSync(file.path);
                            } catch (e) {
                                console.error(e);
                            }
                            if (errRequest) {
                                console.error(errRequest);
                                return resolveP(null);
                            }
                            body = JSON.parse(body);
                            return resolveP(Object.assign(file, {
                                path: body.file
                            }));
                        });
                    });
                });
            })
            .then(f => {
                f = f.filter(v => v !== null);
                const txtFile = f.find(v => (v.fType === 1));
                const htmlFile = f.find(v => (v.fType === 2));
                const attachments = f.filter(v => (v.fType === 3));
                if (f.length === 0) {
                    if (!entryId) {
                        throw new Error('Error upload message');
                    }
                    const sql = `
                    UPDATE Emails
                    SET
                        status = -1
                    WHERE id = ${id}
                    `;
                    return rqS.query(sql).then(() => {
                        throw new Error('Error upload message');
                    });
                }
                if (!entryId) {
                    return f;
                }
                const sql = `
                UPDATE Emails
                SET
                    text = '${JSON.stringify(txtFile)}',
                    html = '${(htmlFile) ? JSON.stringify(htmlFile) : ''}',
                    attachment = '${JSON.stringify(attachments)}',
                    status = 2
                WHERE id = ${id}
                `;
                return rqS.query(sql).then(() => f);
            })
            .then(f => {
                const worker = new Beanstalkd(config.beanstalkd.host, config.beanstalkd.port);
                return worker.connect()
                    .then(() => worker.use(`${config.beanstalkd.prefix}email`))
                    .then(() => worker.put(1, 0, 0, JSON.stringify({ id, to: To, subject: Subject, files: f })))
                    .then(() => worker.quit())
                    .then(() => f);
            })
            .then(f => resolve({ id, to: To, subject: Subject, files: f }))
            .catch(errD => reject(errD));
        });
    });
}
module.exports = sendEmail;
