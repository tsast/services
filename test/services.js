config = require('./fixtures/config');

const path = require('path');

function ok(expr, msg) {
    if (!expr) throw new Error(msg);
}

const services = require('../index');

describe('services', () => {
    describe('#email', () => {
        it('email send', () => {
            const messageParams = {
                name: 'Test name',
                text: 'Test text'
            };
            const messageFiles = {
                path: path.join(__dirname, '02-services.js'),
                type: 'Application/JavaScript',
                name: '02-services.js'
            };
            return services.sendEmail('pma@tsast.ru',
            'Test mail',
            path.join(__dirname, 'fixtures', 'email'),
            messageParams,
            messageFiles);
        });
    });

    describe('#filesToBase64FromDir', () => {
        it('find 02-services base64 from current directory', () => {
            ok(services.filesToBase64FromDir(__dirname).services);
        });
    });

    describe('#includeFilesFromDir', () => {
        it('check config.js required from fixtures', () => {
            ok(services.includeFilesFromDir(path.join(__dirname, 'fixtures')).config);
        });
    });
});
