'use strict';
/**
 * Services
 * @const services
 * @namespace
 */

module.exports = {
    filesToBase64FromDir: require('./lib/filesToBase64FromDir'),
    sendEmail: require('./lib/sendEmail'),
    includeFilesFromDir: require('./lib/includeFilesFromDir'),
    declension: require('./lib/declension')
};
